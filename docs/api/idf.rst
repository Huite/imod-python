imod.idf  -  IDF file I/O
-------------------------

.. automodule:: imod.idf
    :members:
    :undoc-members:
    :show-inheritance:
